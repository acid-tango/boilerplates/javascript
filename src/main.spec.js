import { Main } from "./main"

describe("Default test", () => {
  it("should work", () => {
    const main = new Main("Hello world")
    expect(main.getParam()).toBe("Hello world")
  })

  it("should work", () => {
    expect(Main.COMMANDS).toBe("Hello world")
  })
})
