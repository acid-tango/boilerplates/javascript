/**
 * Main Class
 */
export class Main {
  static get COMMANDS() {
    return "Hello world"
  }

  constructor(param) {
    this.param = param
  }

  getParam() {
    return this.param
  }
}
