# JavaScript Boilerplate

This boilerplate include:

- 📙 Babel 7
- ✅ Jest 24
- 💅 Prettier & ESLint
